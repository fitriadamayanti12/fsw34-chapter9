import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "./pages/home";
import Register from "./pages/register";
import Login from "./pages/login";
import RandomUploader from "./pages/randomUploader";
import Post from "./pages/post";
import PostForm from "./pages/postForm";
import NotFound from "./pages/notFound";

const Router = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/register" element={<Register />} />
      <Route path="/login" element={<Login />} />
      <Route path="/random-uploader" element={<RandomUploader />} />
      <Route path="/post" element={<Post />} />
      <Route path="/post/create" element={<PostForm />} />
      <Route element={<NotFound />} />
    </Routes>
  </BrowserRouter>
);

export default Router;
